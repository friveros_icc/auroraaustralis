var mysql = require('mysql');

/**
 * Módulo que se encarga del acceso a los datos de los registros de los
 * pesos de los lotes capturados en la cinta.
 *
 * @author Francisco Riveros
 * @email friveros.icc@gmail.com
 */
var RegistroPesoDAO = function() {	
	var _connection;

	_init();

	/**
	 * Inicializa la conexión con el servidor
	 */
	function _init() {
		_connection = mysql.createConnection({
			host 		: '127.0.0.1',
			user		: 'aurora_admin',
			password	: 'aurora_admin',
			database	: 'aurora',
			port 		: '3306'
		});

		_connection.connect(function(error) {
				if(error)
					console.log(error);
		});
	}

	/**
	 * Guarda el registro del peso del lote en la base de datos.
	 * @param lote Objeto con la información del código y peso del registro
	 */
	function guardarPeso(lote) {
		var insert 	= 'INSERT INTO lote_peso '
					+ 'VALUES (?, CURRENT_TIMESTAMP(), ?)';

		var values = [lote.codigoLote, lote.peso];

		_connection.query(insert, values, function(err, result) {});
	}

	/**
	 * Guarda la información general del lote.
	 */
	function guardarLote(lote) {
		var insert 	= 	'INSERT INTO lote (codigo_lote, codigo_productor, nombre_productor, tara_total, fecha_ultimo_pesaje) '
					+	'VALUES(?, ?, ?, ?, CURRENT_TIMESTAMP()) ' 
					+	'ON DUPLICATE KEY UPDATE tara_total = ?, fecha_ultimo_pesaje = CURRENT_TIMESTAMP() ';

		var values = [lote.codigoLote, lote.codigoProductor, lote.nombreProductor, lote.taraTotal, lote.taraTotal];

		_connection.query(insert, values, function(err, result) {});				
	}

	/**
	 * Registra en la base de datos el error ocurrido
	 */
	function guardarError(descripcion) {
		var insert 	= 'INSERT INTO error '
					+ 'VALUES (?)';

		var values = [descripcion];

		_connection.query(insert, values, function(err, result) {});
	} 

	/**
	 * Elimina de la base de datos los errores registrados.
	 */
	function eliminarError(success) {
		var remove 	= 'DELETE FROM error ';

		var values = [];

		_connection.query(remove, values, function(err, result) {
			success();
		});
	}

	return {		
		guardarPeso 	: guardarPeso,
		guardarLote 	: guardarLote,
		guardarError	: guardarError,
		eliminarError	: eliminarError
	};
}

module.exports = RegistroPesoDAO;

