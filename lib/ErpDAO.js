var sql = require('mssql');
var _ = require('underscore');

/**
 * Módulo encargado de administrar el acceso a los datos del ERP
 *
 * @author Francisco Riveros
 * @email friveros.icc@gmail.com
 */
var ErpDAO = function() {
	var _config;
	var _connection;

	_init();

	/**
	 * Inicializa las variables globales.
	 */
	function _init() {
		_config = {
			user		: 'emerino',
			password	: '152402',
			server		: '192.168.0.3'
		}		
	}

	/**
	 * Busca la información del lote (productor y taras totales).
	 */
	function buscarLote(codigoLote, success, error) {
		var connection = new sql.Connection(_config, function(err) {		
			var request = connection.request();
			var select 	= 	'SELECT		rec.cod_pro, pro.nom_pro, '
						+ 				'ROUND(ISNULL(( '
						+					'SELECT 	SUM(cta.cantidad * env.peso_neto) tara_total '
						+					'FROM 		detalle_ctacte_envases cta, ' 
						+								'envase env '
						+ 					'WHERE 		cta.cod_tem = rec.cod_tem '
						+					'AND 		cta.COD_EMP = rec.COD_EMP ' 
						+					'and cta.LOTE = rec.LOTE '
						+					'and cta.TIPO_MOV = \'0\' '
						+					'and env.cod_emp = cta.cod_emp '
						+ 					'and env.cod_tem = cta.cod_tem '
						+					'and env.cod_env = cta.cod_env '
						+				'), 0), 1) AS tara_total '
						+	'FROM	recepack	rec, '
						+			'PRODUCTORES pro '
						+	'WHERE	rec.lote = \'' + codigoLote + '\' '
						+	'AND		pro.COD_PRO = rec.COD_PRO '
						+	'AND		pro.COD_TEM = rec.COD_TEM '
						+	'AND		pro.COD_EMP = rec.COD_EMP ';			

			request.query(select, function(err, recordset) {
				if(_.isUndefined(recordset) || _.isEmpty(recordset)) {
					error("El lote "+ codigoLote +", recien pesado, no existe en los registros de ErpFrusys");
					return false;
				}
				var lote = {
					codigoLote 		: codigoLote,
					codigoProductor : recordset[0].cod_pro,
					nombreProductor : recordset[0].nom_pro,
					taraTotal 		: recordset[0].tara_total
				};
								
				success(lote);
			});
		});
	}	

	return {
		buscarLote : buscarLote
	};
};

module.exports = ErpDAO;
