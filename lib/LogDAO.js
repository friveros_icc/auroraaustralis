var mysql = require('mysql');

/**
 * Módulo que se encarga del acceso a los datos de los registros 
 + eventos del sistema.
 *
 * @author Francisco Riveros
 * @email friveros.icc@gmail.com
 */
var LogDAO = function() {	
	var _connection;

	_init();

	/**
	 * Inicializa la conexión con el servidor
	 */
	function _init() {
		_connection = mysql.createConnection({
			host 		: '127.0.0.1',
			user		: 'aurora_admin',
			password	: 'aurora_admin',
			database	: 'aurora',
			port 		: '3306'
		});

		_connection.connect(function(error) {
				if(error)
					console.log(error);
		});
	}

	/**
	 * Guarda el registro del evento del sistema.
	 *
	 */
	function guardarEvento(evento) {
		var insert 	= 'INSERT INTO log (fecha, descripcion) '
				+ 'VALUES (CURRENT_TIMESTAMP(), ?)';

		var values = [evento];

		_connection.query(insert, values, function(err, result) {});
	}

	return {		
		guardarEvento 	: guardarEvento
	};
}

module.exports = LogDAO;

