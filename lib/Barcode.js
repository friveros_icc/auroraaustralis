var HID = require('node-hid');
var _ = require('underscore');

/**
 * Módulo encargado de gestionar la lectura de información desde el lector
 * de código de barra.
 * 
 * @author Francisco Riveros
 * @email friveros.icc@gmail.com
 * 
 */
var Barcode = function(callback) {
	var _codigos = [];		
	var _codigoBarra1 = undefined;
	var _codigoBarra2 = undefined;
	var _timestamp1 = 0;
	var _timestamp2 = 0;
	var _callback = callback; 	// Función que es llamada cuando es leido el código de barras.
	var _timestampPush = 0;
	var _timestampSecond = 0;


	_init();

	/**
	 * Inicializa la conexión con el lector de códigos de barra.
	 */
	function _init() {				
		var devices = HID.devices();
		var deviceWireless;
		
		// Ordenamos los dispositivos
		_.sortBy(devices, function(dev) { return dev.serialNumber; });

		// Buscamos los lectorres de código de barras.
		var barcodeDevices = _.filter(devices, function(device) { 
			if(device.serialNumber == 'S/N:9997C44FE94E244B85C44DBF16198234 Rev:NBCACAAN3')
				deviceWireless = new HID.HID(device.path);
			if(device.vendorId === 1504 && device.productId === 4608)
				return device;
		});				

		// Abrimos la conexión al lector de barras.
		var device1 = new HID.HID(barcodeDevices[0].path);
		var device2 = new HID.HID(barcodeDevices[1].path);
		
		// Nos ponemos a la escucha de información desde el lector.
		device1.on('data', _processData1);
		device2.on('data', _processData2);
		deviceWireless.on('data', _processData1);
	}

	/**
	 * Función que se encarga de convertir los datos de Hexadecimal a Char.
	 */
	function _hexToChar(data) {
		var numero = '';
		
		switch(data[2]) {
			case 30:
				numero = '1';
				break;
			case 31:
				numero = '2';
				break;
			case 32:
				numero = '3';
				break;
			case 33:
				numero = '4';
				break;
			case 34:
				numero = '5';
				break;
			case 35:
				numero = '6';
				break;
			case 36:
				numero = '7';
				break;
			case 37:
				numero = '8';
				break;
			case 38:
				numero = '9';
				break;
			case 39:
				numero = '0';
				break;
		}
		
		return numero;	
	}

	/**
	 * Se encarga de evaluar si el código ha sido leido completo.
	 */
	function _evaluarCodigo1() {
		setTimeout(function() {
			if(_.isUndefined(_timestamp1) === false) {
				var diff = Date.now() - _timestamp1;

				if(diff >= 400) {
					if((Date.now() - _timestampPush) > 1000) {
						if(!_.isEmpty(_codigoBarra1) && !_.isUndefined(_codigoBarra1)) {
							if(_codigoBarra1.length >= 6) {
								_timestampPush = Date.now();
								_codigos.push(_codigoBarra1);	
								_callback();
								console.log(_codigos);
							}
						}
					}
				}
			}
		}, 500);
	}

	function _evaluarCodigo2() {
		setTimeout(function() {
			if(_.isUndefined(_timestamp2) === false) {
				var diff = Date.now() - _timestamp2;

				if(diff >= 400) {
					if((Date.now() - _timestampSecond) > 1000) {
                                                _timestampSecond = Date.now();
                                                //_callback();
                                        }                                      	
				} 															
			}
		}, 500);
	}

	/**
	 * Procesa la información extraída desde el lector de código de barras.
	 */
	function _processData1(data) {		
		var localtimestamp = Date.now();
		if(_.isUndefined(_codigoBarra1) || (localtimestamp - _timestamp1) > 100)
			_codigoBarra1 = '';
		_codigoBarra1 += _hexToChar(data);

		console.log('process -> '+_codigoBarra1);
		_timestamp1 = Date.now();	

		// Evaluamos si esta completo el código de barra.
		_evaluarCodigo1();	
	}	

	/**
	 * Procesa la información extraída desde el lector de código de barras.
	 */
	function _processData2(data) {
		var localtimestamp = Date.now();

		if(_.isUndefined(_codigoBarra2) || (localtimestamp - _timestamp2) > 100)
			_codigoBarra2 = '';
		_codigoBarra2 += _hexToChar(data);
		
		_timestamp2 = Date.now();	

		// Evaluamos si esta completo el código de barra.
		_evaluarCodigo2();	
	}	

	/**
	 * Retorna el código de barra leído desde el dispositivo.
	 */
	function getBarcode1() {
		return _codigoBarra1;
	}

	function getUltimoCodigo() {
		console.log(_codigos);
		return _codigos.shift();
	}
	
	function removerUltimoCodigo() {
		_codigos.pop();
	}

	function getBarcode2() {
		return _codigoBarra2;
	}

	function removerCodigo2() {
		_codigoBarra2 = undefined;
	}

	/**
	 * Borra la lectura del úlimo código.
	 */
	function resetBarcode() {
		_codigoBarra1 = undefined;
		_codigoBarra2 = undefined;
	}

	/**
	 * Devuelve la fecha-hora en que fue leido el último código de barra.
	 */ 
	function getTimestamp() {
		return _timestamp1;
	}
	
	function getPilaCodigos() {
		return _codigos;
	}

	return {
		getBarcode1 		: getBarcode1,
		getBarcode2 		: getBarcode2,
		resetBarcode 		: resetBarcode,
		getTimestamp		: getTimestamp,
		removerUltimoCodigo 	: removerUltimoCodigo,
		removerCodigo2 		: removerCodigo2,
		getUltimoCodigo 	: getUltimoCodigo,
		getPilaCodigos 		: getPilaCodigos 
	};
}

module.exports = Barcode;
