var com = require("serialport");
var Barcode = require("./Barcode");

/** 
 * Módulo que se encarga de la conexión con la tarjeta Arduino
 * el cual posee el sensor de proximidad y la función de detener el motor.
 *
 * @author Francisco Riveros
 * @email friveros.icc@gmail.com
 */
var Arduino = function(callback) {		
	var _timestamp = 0;
	var _lastTimestamp = Date.now();
	var _cantidadCeros = 0;

	var _serialPort = new com.SerialPort("/dev/ttyACM0", {
		baudrate	: 57600,
		parser 		: com.parsers.readline('\r\n')
	});	

	// Función ejecutada al momento de que es detectado un objeto frente al sensor.
	var _callback = callback;

	_init();

	/**
	 * Nos conectamos a la placa arduino y quedamos a la escucha de datos.
	 */
	function _init() {		
		_serialPort.on('data', _processData);
	}

	/**
	 * Procesa la información recivida desde el sensor de próximidad.
	 */
	function _processData(distance) {						
		var localTimestamp = Date.now();
		//console.log(distance);						
		if(distance === '0' ) {
			_cantidadCeros++;			
			//console.log("cantidad ->> "+_cantidadCeros);
			if(_cantidadCeros === 4 ) {
				if((localTimestamp - _lastTimestamp) > 1000) { 				
					_timestamp = Date.now();
					//console.log("hay algo");

					_callback();
				}		_lastTimestamp = Date.now();
			
			

				//_lastTimestamp = localTimestamp;
			}
		} else {
			_cantidadCeros = 0;
		}
	};	

	/**
	 * Para los motores de la línea.
	 */
	function stopMotor() {			
		_serialPort.write('1', function(err, results) {});
	}

	function startAlarm() {
		_serialPort.write('2', function(err, result) {});
	}

	function stopAlarm() {
		_serialPort.write('3', function(err, result) {});
	}

	/**
	 * Devuelve la fecha-hora de la última lectura del sensor.
	 */
	function getTimestap() {
		return _timestamp;
	};

	return {		
		stopMotor 		: stopMotor,	
		startAlarm 		: startAlarm, 	
		stopAlarm 		: stopAlarm,
		getTimestamp 		: getTimestap
	};
}

/*var aux;
function time() {
	aux = Date.now();
}

function time2() {
	console.log(Date.now() - aux);
}
*/
// var arduino = Arduino(function(){});
//arduino.stopAlarm();
//arduino.startAlarm();
//var barcode = Barcode(time2);

//var arduino = Arduino(function() { arduino.stopMotor();});
module.exports = Arduino;
