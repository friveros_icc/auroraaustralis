var mysql = require('mysql');

// Establecemos los parámetros de la conexión.
var connection = mysql.createConnection({
	host 		: '127.0.0.1',
	user		: 'aurora_admin',
	password	: 'aurora_admin',
	database	: 'aurora',
	port		: '3306'
});

// Nos conectamos a la base de datos.
connection.connect(function(error) {
	if(error) {
		throw error;
	} else {
		console.log('Conexión correcta.');
	}
});

var select = 'SELECT * FROM lote_peso';

// Ejecutamos la consulta a la base de datos.
var query = connection.query(select, [], function(error, result) {
	if(error) {
		throw error;
	} else {
		for(var i = 0; i < result.length; i++)
			console.log(result[i]);
	}
});