var com = require("serialport");

/**
 * Módulo encargado de leer la información desde la pesa
 * instalada en la línea.
 *
 * @author Francisco Riveros.
 * @email friveros.icc@gmail.com
 */
var Scale = function(callback) {
	var _callback = callback; // Función que es llamada al momento de que es capturado el peso.

	/** 
	 * Creamos el objeto con la información del puerto serial
	 * a utilizr.
	 */
	var _serialPort = new com.SerialPort("/dev/ttyS0", {
		baudrate	: 9600,
		parser 		: com.parsers.readline('\r\n')
	});

	var _peso = 0;
	var _timestamp = 0;

	_init();

	/** 
	 * Inicializa la conexión con la pesa.
	 */
	function _init() {
		_serialPort.on('data', _processData);	
	}

	/**
	 * Procesa los datos capturados desde la pesa.
	 */
	function _processData(data) {
		var localTimestamp = Date.now();

		if(data.indexOf('GROSS') > -1) {
			if((localTimestamp - _timestamp) > 1000) {
				var array = data.split(' ');
				var txt = array[array.length - 1];
				_peso = txt.substring(0, txt.length - 2); 				
				_timestamp = 0;
				_callback();
			}
			_timestamp = localTimestamp;
		}
		
	}	

	/**
	 * Devuelve la información del peso capturado por la balanza.
	 */
	function getPeso() {
		return _peso;
	}

	function resetPeso() {
		_peso = 0;
	}

	return {		
		getPeso 	: getPeso,
		resetPeso 	: resetPeso
	};
};

module.exports = Scale;
