var reboot			= require('reboot');
var _ 				= require('underscore');
var Arduino 		= require('./lib/Arduino')(esperarCodigoBarra);
var Barcode 		= require('./lib/Barcode')(verificarCodigos);
var Scale 			= require('./lib/Scale')(capturarPeso);
var RegistroPesoDAO = require('./lib/RegistroPesoDAO')();
var ErpDAO 			= require('./lib/ErpDAO')();
var LogDAO 			= require('./lib/LogDAO')();

var tiempo			= 6;
var codigoBarra 	= undefined;
var timstampError  	= 0;
var descartarPeso 	= -1;
var codigoPrimario 	= true;
var contadorPeso 	= 0;
var contadorSensor	= 0;
var ultimaLectura 	= Date.now();

LogDAO.guardarEvento('Sistema iniciado ...');

verificarReinicio();

/**
 * Deja en espera un proceso para verificar si es leído
 * el código del lote, luego de un tiempo determinado.
 */
function esperarCodigoBarra() {		
	LogDAO.guardarEvento('Esperando codigo');
	contadorSensor++;
	ultimaLectura = Date.now();

	Barcode.resetBarcode();	
	RegistroPesoDAO.eliminarError(function() {});
	Arduino.stopAlarm();

	// Esperamos la cantidad de tiempo necesario para poder capturar 
	// el código de barra.	
	setTimeout(function() {		
		verificarCapturaCodigoBarra();
	}, (tiempo * 1000));

	verificarReinicio();
}

/**
 * Función encargada de verificar si a pasado el tiempo máximo para reeiniciar el 
 * sistema
 */
function verificarReinicio() {
	//console.log('Esperando verificacion');
	setTimeout(function() {
		var diff = Date.now() - ultimaLectura;
		
		LogDAO.guardarEvento('Diff reinicio -> '+diff);
		
		if(diff >= 1150000) {
			LogDAO.guardarEvento('Reiniciando sistema por inactividad ...'); 
			reboot.reboot(); 
		}
	}, 1200000);
}

/**
 * Comprueba que el sensor a detectado un elemento y si se ha 
 * leido un código de barra.
 */
function verificarCapturaCodigoBarra() {

	var codigo = Barcode.getBarcode1();		
	
	if(Arduino.getTimestamp() > Barcode.getTimestamp()) {
		if(_.isUndefined(codigo)) {				
			guardarError("No ha sido posible leer el código de barra del lote");			
		}
	}
}

/**
 * Verifica que los códigos capturados por los lectores
 * de códigos de barra sean iguales, en caso contrario 
 * el la cinta es detenida.
 */
function verificarCodigos() {	
	var cod1 = Barcode.getBarcode1();
	var cod2 = Barcode.getBarcode2();

	var paraMotor = false;
	var msg = 'cod1 : ' +  cod1 + ', cod2 : ' + cod2;
	LogDAO.guardarEvento(msg);

	if(!_.isUndefined(cod2) && _.isUndefined(cod1)) {
		codigoPrimario = false;
		setTimeout(verificarCodigoPrimario, 1000);
	}

	if(!_.isUndefined(cod1))
		codigoPrimario = true;

	if(!_.isUndefined(cod1) && !_.isUndefined(cod2)) {
		if(cod1.length === cod2.length) {
			if(cod1 < cod2 || cod1 > cod2)
				paraMotor = true;
			else 
				Barcode.resetBarcode();
		}
	}
	if(paraMotor) {
		Barcode.resetBarcode();		
		Barcode.removerUltimoCodigo();
		descartarPeso = contadorSensor;
		guardarError("Se han leido códigos de barra distintos antes de pesar");
	} else {				
		LogDAO.guardarEvento('Lectura codigo OK');
	}

	Barcode.resetBarcode();	
}

function verificarCodigoPrimario() {
	if(!codigoPrimario) {
		guardarError("No ha sido posible leer el código de barra del Bins inferior");
	}
}

/**
 * Lee la información del peso entregado por la balanza.
 */
function capturarPeso() {	
	contadorPeso++;
	if(descartarPeso === contadorPeso) {
		descartarPeso = -1;
		guardarError('No fue leído el código de barra del Bins pesado');
	} else {
		codigoBarra = Barcode.getUltimoCodigo();		
		//console.log(Barcode.getPilaCodigos());

				
		if(!_.isUndefined(codigoBarra)) 
			guardarPeso(codigoBarra);
		else
			guardarError('No fue leído el código de barra del Bins pesado');			

		var msg = '[';
		_.each(Barcode.getPilaCodigos(), function(codigo) {
			msg += codigo+',';
		});
		msg += ']';

		LogDAO.guardarEvento(msg);
	}		
}

/**
 * Guarda la información del peso capturado por la pesa,
 * junto con la información del lote en la BD del sistema.
 */
function guardarPeso(codigoBarra) {	
	// Buscamos la información del lote en el ERP.
	ErpDAO.buscarLote(codigoBarra, function(data) {
		var lote = data;
		lote.peso = Scale.getPeso();
	
		var msg = 'GUARDADO -> lote : '+lote.codigoLote+', peso : '+lote.peso;
		LogDAO.guardarEvento(msg);
		
		//Barcode.resetBarcode();
		Scale.resetPeso();				

		RegistroPesoDAO.guardarPeso(lote);
		RegistroPesoDAO.guardarLote(lote);

	}, guardarError);	
}

/**
 * Registra en la base de datos los errores
 * generados en el sistema.
 */
function guardarError(msg) {
	var peso = Scale.getPeso();

	//console.log(msg + " -> peso ["+peso+"]");
	LogDAO.guardarEvento(msg+' -> peso ['+peso+']');
	Arduino.startAlarm();
	Arduino.stopMotor();
	RegistroPesoDAO.eliminarError(function() {
		RegistroPesoDAO.guardarError(msg);
	});	
}

